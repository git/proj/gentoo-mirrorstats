#!/usr/bin/python

#
# Convert a mirmon.state file to json
#
# This script will parse a mirmon state file, convert the content
# to json and print the json to stdout.
#
# Usage:
#   $ generate-json.py path_to_mirmon_state_file
#
# The json will be printed to stdout. In case of an error, the
# exception will be printed to stderr.
#

import json
import sys
import time

try:
    with open(str(sys.argv[1])) as file_in:
        mirrors = {}
        for raw_line in file_in:

            line = raw_line.split(" ")
            protocol = line[0].split("://")[0]
            host = line[0].split("://")[1]
            host = host.split("/")[0]

            state = {
                "Protocol" : protocol,
                "Host" : host,
                "Url" : line[0],
                "Age" : line[1],
                "StatusLastProbe" : line[2],
                "TimeLastSuccessfulProbe" : line[3],
                "ProbeHistory" : line[4],
                "StateHistory" : line[5],
                "LastProbe" : line[6].strip(),
            }

            if host not in mirrors:
                mirrors[host] = []

            mirrors[host].append(state)

        data = {}
        data["LastUpdate"] = int(time.time())
        data["Mirrors"] = mirrors
        print(json.dumps(data))

except Exception as e:
    print("Could not convert mirmon state file to json: " + str(e), file=sys.stderr)
