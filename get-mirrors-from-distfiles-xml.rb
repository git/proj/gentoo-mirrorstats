#!/usr/bin/ruby
require 'rexml/document'
require 'open-uri'

MIRROR_DATA="https://api.gentoo.org/mirrors/distfiles.xml"

m = URI.parse(MIRROR_DATA).read
x = REXML::Document.new(m)

def normalize_mirror(xml_elem)
  return xml_elem.texts().join(' ').sub(/\/+$/, '') + '/'
end

def select_mirror(xml_elem)
  1
end

REXML::XPath.each(x, '//*/mirrorgroup[@country]') {|el|
  country = el.attributes['country']

  el.each_element('mirror/uri/') do |uri_elem|
    puts "#{country.downcase} #{normalize_mirror(uri_elem)}" if select_mirror(uri_elem)
  end
}
