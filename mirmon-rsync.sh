#!/bin/bash
exec $(dirname "$0")/mirmon-wrapper.sh \
	--quiet \
	--mode=rsync \
	--xml-src=rsync \
	--url=https://www.gentoo.org/support/rsync-mirrors/
