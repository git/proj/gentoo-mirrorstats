#!/bin/bash
exec $(dirname "$0")/mirmon-wrapper.sh \
	--quiet \
	--mode=snapshots \
	--xml-src=distfiles \
	--url=https://www.gentoo.org/downloads/mirrors/
