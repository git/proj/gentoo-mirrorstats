#!/bin/bash -e

NAME=$(basename "$0")

OPTIONS_STR=(
	"mode"
	"xml-src"
	"url"
)
OPTIONS_BOOL=(
	"help"
	"debug"
	"verbose"
	"quiet"
)


helpmsg() {
	echo "$*" 1>&2
}

usage() {
	helpmsg "Usage: ${NAME}$(printf " --%s=..." "${OPTIONS_STR[@]}" )$(printf " [--%s]" "${OPTIONS_BOOL[@]}")"
	exit 3
}

dohelp() {
	helpmsg "${NAME} TODO"
	exit 0
}

die() {
	helpmsg "$*"
	exit 2
}

main() {
	SITEDIR=/var/www/mirrorstats.gentoo.org
	REPODIR=${SITEDIR}/gentoo-mirrorstats/
	CONFDIR=${REPODIR}/conf/${MODE}/
	VARDIR=${SITEDIR}/var/${MODE}
	HTDOCS=${SITEDIR}/htdocs/${MODE}
	MIRMON=$(readlink -f "${SITEDIR}"/mirmon/mirmon)
	CONFFILE=$(readlink -f "${CONFDIR}"/mirmon.conf)
	VAR_GMIRRORS=${VARDIR}/g.mirrors
	EXTRA_MIRRORS=${CONFDIR}/g.mirrors.extra
	# Lock outselves
	if [ "${FLOCKER}" != "$0" ]; then
		exec \
		env FLOCKER="$0" flock -en "${VARDIR}/flock" \
		"$0" \
		${DEBUG:+--debug} \
		${QUIET:+--quiet} \
		${VERBOSE:+--verbose} \
		"--xml-src=${XML_SRC}" \
		"--mode=${MODE}" \
		"--url=${URL}"
	fi

	# Grab mirrors from the web
	mkdir -p "${VARDIR}" "${HTDOCS}" || die "Failed to mkdir"
	"${REPODIR}/get-mirrors-from-${XML_SRC}-xml.rb" > "${VAR_GMIRRORS}".tmp || die "Unable to fetch mirror list"

	[[ -e "${EXTRA_MIRRORS}" ]] && cat "${EXTRA_MIRRORS}" >>"${VAR_GMIRRORS}".tmp

	# Atomic rotate into place
	mv -f "${VAR_GMIRRORS}"{.tmp,} || die "Failed to set mirror list"

	# fatal if the state file is NOT present.
	if [[ ! -e ${VARDIR}/mirmon.state ]]; then
		touch "${VARDIR}/mirmon.state" || die "Could not create state file"
	else
		test -w "${VARDIR}/mirmon.state" || die "Cannot write state file!"
	fi

	MIRMON_OPTS=(
		 -c "${CONFFILE}"
		 -get update
	)
	[[ $VERBOSE -eq 1 ]] && MIRMON_OPTS+=( '-v' )
	[[ $DEBUG -eq 1 ]] && MIRMON_OPTS+=( '-d' )
	[[ $QUIET -eq 1 ]] && MIRMON_OPTS+=( '-q' )

	# run mirmon
	/usr/bin/perl "${MIRMON}" "${MIRMON_OPTS[@]}" || die "mirmon failed: $?"

	# Set up a nice link to our mirror page directly:
	sed \
	    -e "s#mirrors</H2>#<a href=\"${URL}\">mirrors</a></H2>#" \
	    >"${HTDOCS}"/index.html.tmp <"${HTDOCS}"/index-wip.html \
		|| die "Failed to fix URLs(sed)"
	mv -f "${HTDOCS}"/index.html{.tmp,} || die "Failed to fix URLs(mv)"

	# Generate a json file containing the state of each mirror
	"${REPODIR}"/generate-json.py \
		"${VARDIR}"/mirmon.state \
		>"${HTDOCS}"/state.json.tmp \
	|| die "Failed to generate JSON (py)"
	mv -f "${HTDOCS}"/state.json{.tmp,} || die "Failed to generate JSON (mv)"

	# Done
	exit 0
}

opts=$(getopt \
	--longoptions "$(printf "%s:," "${OPTIONS_STR[@]}" )" \
	--longoptions "$(printf "%s," "${OPTIONS_BOOL[@]}" )" \
	--name "$(basename "$0")" \
	--options "" \
	-- "$@"
)

eval set --$opts

MODE=
XML_SRC=
URL=
HELP=
DEBUG=
VERBOSE=
INVALID=

while [[ $# -gt 0 ]]; do
    case "$1" in
		# Strs:
        --mode)
            MODE=$2
            shift 2
            ;;

        --xml-src)
            XML_SRC=$2
            shift 2
            ;;
        --url)
            URL=$2
            shift 2
            ;;
		# Bools:
		--quiet)
			QUIET=1
			shift
			;;
		--debug)
			DEBUG=1
			shift
			;;
		--verbose)
			VERBOSE=1
			shift
			;;
		--help)
			HELP=1
			shift
			;;
		# End of options
		--)
			break
			;;
        *)
			INVALID=1
            break
            ;;
    esac
done

# Validations
MODE_INPUTS=(
	distfiles
	rsync
	snapshots
	experimental
	releases
)
MODE_VALID=0
for m in "${MODE_INPUTS[@]}"; do
	[[ "$m" == "$MODE" ]] && MODE_VALID=1
done
[[ $MODE_VALID -eq 1 ]] || MODE=''

XML_SRC_INPUTS=(
	distfiles
	rsync
)
XML_SRC_VALID=0
for m in "${XML_SRC_INPUTS[@]}"; do
	[[ "$m" == "$XML_SRC" ]] && XML_SRC_VALID=1
done
[[ $XML_SRC_VALID -eq 1 ]] || XML_SRC=''

[[ "$INVALID" == 1 ]] && usage
[[ "$HELP" == 1 ]] && dohelp

if [[ -z "$MODE" ]]; then helpmsg "--mode must be one of: ${MODE_INPUTS[*]}" ; INVALID=1 ; fi
if [[ -z "$XML_SRC" ]]; then helpmsg "--xml-src must be one of: ${XML_SRC_INPUTS[*]}" ; INVALID=1 ; fi
if [[ -z "$URL" ]]; then helpmsg "--url unset" ; INVALID=1 ; fi
[[ "$INVALID" == 1 ]] && usage

main
