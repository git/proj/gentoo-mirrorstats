#!/bin/bash
# Check if the mirror is alive and not lagging.
# The script performs three checks, and it proceeds only when the check fails:
# 1) Check if the mirror is fully clean, else proceed
# 2) Check if the latest attempts where clean (five for distfiles, one for
#    rsync), else proceed
# 3) Check if it had more than 10 failed attempts. If it does, then print
#    the mirror, else skip

. /etc/init.d/functions.sh

case $1 in
	distfiles) ;;
	rsync) ;;
	releases) ;;
	snapshots) ;;
	experimental) ;;
	*)
    echo "Need to specify valid mode" 1>&2
    exit 1
	;;
esac

STATE_FILE="/var/www/mirrorstats.gentoo.org/var/$1/mirmon.state"

count_bad_status() {
    BAD_STATUS_COUNT=0
    i=0
    while (( i++ < ${#FULL_STATUS} )); do
        echo $(expr substr "$FULL_STATUS" $i 1) | grep -v -q s && \
            BAD_STATUS_COUNT=$((BAD_STATUS_COUNT + 1))
    done
    [ $BAD_STATUS_COUNT -ge 10 ] && echo $NAME
}

while read mirror; do
    NAME=`echo $mirror | cut -d' ' -f1`
    FULL_STATUS=`echo $mirror | cut -d' ' -f6 | cut -d'-' -f2`
    if [[ $1 == "distfiles" ]]; then
        LATEST_STATUS="${FULL_STATUS:9:5}"
        LATEST_STATUS_EXP="sssss"
    elif [[ $1 == "rsync" ]]; then
        LATEST_STATUS="${FULL_STATUS:13:1}"
        LATEST_STATUS_EXP="s"
    fi
    if [[ $FULL_STATUS != "ssssssssssssss" ]]; then
        if [[ $LATEST_STATUS != $LATEST_STATUS_EXP ]]; then
            count_bad_status
        fi
    fi
done < $STATE_FILE
